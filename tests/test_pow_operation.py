from polls_project.math_operations.pow_operation import PowOperation


def test_pow_1_2_should_be_1():
    # given
    a = 1
    b = 2
    expected = 1
    pow_object = PowOperation()
    # when
    result = pow_object.pow(a, b)
    # then
    assert result == expected
