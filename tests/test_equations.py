from polls_project.math_operations.operations import Operation
from polls_project.math_operations.sub_operation import SubOperation
from polls_project.math_operations.sum_operation import SumOperation


def test_equations():
    # 1 + 2 - 3 = 0
    # given
    a = 1
    b = 2
    c = 3
    expected = 0
    # when
    interm = SumOperation().sum(a, b)
    result = SubOperation().sub(interm, c)
    # then
    assert result == expected


def test_operation_equation():
    # given
    a = 1
    b = 2
    c = 3
    expected = 0
    # when
    operation1: Operation = SumOperation()
    operation2: Operation = SubOperation()
    interm = operation1.compute(a, b)
    result = operation2.compute(interm, c)
    # then
    assert result == expected
