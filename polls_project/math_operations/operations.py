from abc import ABC, abstractmethod


class Operation(ABC):
    @abstractmethod
    def compute(self, first_operand, second_operand):
        pass
